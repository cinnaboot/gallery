
import os

from flask import Flask

from .views import default_routes


def create_app():
    app = Flask(__name__, instance_relative_config=True)

    # NOTE: ensure intance folders exist
    try: os.makedirs(app.instance_path)
    except OSError: pass
    try: os.makedirs(os.path.join(app.instance_path, 'images'))
    except OSError: pass
    try: os.makedirs(os.path.join(app.instance_path, 'thumbnails'))
    except OSError: pass

    app.register_blueprint(default_routes.bp)
    return app

