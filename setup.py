
from setuptools import setup

setup(
    name='gallery',
    version='0.1',
    packages=['gallery'],
    include_package_data=True,
    install_requires=['flask', 'pillow']
)

